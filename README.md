# RCA monitoring survey design

__Authors:__  Jessica Nephin    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [References](#references)


## Objective
To locate appropriate control areas for each RCA and generate spatially balanced transects within paired RCA and control areas.

## Summary
To monitor the effectiveness of RCAs in meeting their conservation objectives, we need to collect observations of the monitoring indicators/metrics within and outside of RCAs at similar sites. To acheive this, we locate control areas for each RCA that are nearby their paired RCA in geographic space, match the depth range of their paired RCA, and sized relative to their paired RCA. Then we generate spatially balanced transects within RCAs and control areas using a BAS algorithm to sample the starting points of transects. The resulting transects are more likely to fall within higher complexity areas with greater pre RCA fishing effort and transects will follow
depth contours where possible. The code is intended to be used to generate sampling locations for multiple sampling gear types (e.g., ROV, SCUBA, BRUV) and transect length, depth ranges, number of transects can be adapted if needed.

## Status
In-development

## Contents
This repository includes R code to prepare the input spatial data layers, select the control areas, and generate the transects.

## Methods
### Control areas
1. Buffer RCAs by the mean distance from the center of an RCA to its boundary (with some adjustments on buffer size for small or long/thin RCAs).
2. Crop buffered areas based on depth using the minimum and maximum depth within each RCA (with some adjustments where max depth was shallower than 30 m).
3. Crop out all protected area (including RCAs and sponge reef closures) and land to create reference area
### Generate transects
1. Selects the number of transects to sample by scaling the number of transects by RCA size between a predefined max and min number of transects.
2. Sets inclusion probabilities for BAS algorithm by combining complexity and pre RCA fishing effort into a single layer scaled from 0 to 1.
3. Sets depth range for sampling as +/ 20 m depth for 2 depth bins and up to +/ 40 m for a single depth bin. Depth bins are referenced from the mean depth of the RCA where inclusion probabilities are greater than the median.
4. Samples transect start points by running BAS algorithm with unequal inclusion probabilities within set depth range (that varies by RCA).
5. Draws transects by iteratively selecting adjacent cells until the desired length is reached. Cells are selected by prioritizing cells with the highest inclusion probabilities within a similar depth as the previous transect cell.

## Requirements
R version 4.2.1    
Terra 1.7-55 (R package)    
MBHDesign 2.3.15 (R package)   
sf 1.0-14 (R package)    

## Caveats

## References
